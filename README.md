# Vue3-All-Structure

this vue3-all-structure is setting the default structure for all developer uses of this.

this structure have

- vue-router v4.x.x
- store from Pinia
- css preprocess (scss)
- Tailwind CSS

## Getting started

after your clone this vue 3 files structure your just follow this command

go in to directory project

```
cd vue3-all-structure

```

install all dependencies

```
yarn

```

run vue3 for developing

```
yarn dev

```
